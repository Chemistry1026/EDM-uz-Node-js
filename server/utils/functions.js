require('dotenv').config();

module.exports = {
    url(path) {
        return `${process.env.APP_URL}/${path}`;
    },

    validateURL(str) {
        var pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
        return !!pattern.test(str);
    },

    json_data(data = [], message = '') {
        return {
            data,
            message,
        }
    },

    hashPassword(password) {
        const bcrypt = require('bcrypt');
        const saltRounds = parseInt(process.env.SALT);
        return bcrypt.hash(password, saltRounds);
    },

    checkPassword(password, hash) {
        const bcrypt = require('bcrypt');
        return bcrypt.compare(password, hash);
    },

    async makeFaker(callback, count = 15) {
        let items = [];
        const faker = require('faker/locale/ru');
        for (let i = 0; i < count; i++) {
            items.push(await callback(faker));
        }

        return items;
    },

    makeJWT(id) {
        const jwt = require('jsonwebtoken');
        let payload = {id};
        let token = jwt.sign(payload, process.env.JWT_SECRET);
        return {
            access_token: token,
            message: 'Вы успешно авторизовались',
        };
    },

    errors_data(errors, message = "The given data was invalid.") {
        return {}
    },

    fileExists(file) {
        const fs = require('fs');
        return fs.existsSync(file);
    },

    parsePath(file) {
        const path = require('path');
        return path.parse(file);
    },
};
