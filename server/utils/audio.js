const shell = require('shelljs');
const {fileExists, parsePath} = require('./functions');

module.exports = {
    async ffmpegExists() {
        return await shell.which('ffmpeg');
    },

    changeBitrate(file, bitrate = 128) {
        if (!this.ffmpegExists()) {
            return 'fffmpeg не найден!';
        }

        if (!fileExists(file)) {
            return 'Файл не найден!';
        }

        let parse = parsePath(file);
        return shell.exec(`ffmpeg -i '${file}' -vn -ar 44100 -ac 2 -b:a ${bitrate}k '${parse.dir}/${parse.name}-128${parse.ext}' > /dev/null 2>/dev/null &`);
    },

    makeWaveForm(file) {
        if (!ffmpegExists()) {
            return 'fffmpeg не найден!';
        }

        if (!fileExists(file)) {
            return 'Файл не найден!';
        }

        let parse = parsePath(file);
        return shell.exec(`ffmpeg -y -i '${file}' -filter_complex 'aformat=channel_layouts=mono,compand,showwavespic=s=600x120,crop=in_w:in_h/2:0:0' -c:v png -pix_fmt monob -frames:v 1 '${parse.dir}/${parse.name}-waveform.png' > /dev/null 2>/dev/null &`);
    },
};
