const User = require('../models').User;
const passport = require('passport');
const PassportJWT = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
require('dotenv').config();

let JWTOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET,
};
passport.use(new PassportJWT(JWTOptions,
    (payload, next) => {
        User.findOne({
            where: {
                id: payload.id
            },
            attributes: ['id']
        }).then((user) => {
            return next(null, user.dataValues)
        }).catch(err => {
            return next(null, false);
        });
    }
));

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
    User.findOne({
        where: {
            id
        }
    }).then((user) => {
        return cb(null, user)
    }).catch(err => {
        return cb(err);
    });
});

module.exports = passport;
