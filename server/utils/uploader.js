module.exports = (express, app) => {
    const tus = require('tus-node-server');
    const EVENTS = tus.EVENTS;
    const server = new tus.Server();
    const cors = require('cors');
    const passport = require('passport');

    server.datastore = new tus.FileStore({
        directory: 'uploads/stored',
        path: '/stored',
    });

    server.on(EVENTS.EVENT_UPLOAD_COMPLETE, (event) => {
        console.log(event);
    });

    let jwtConfig = {
        session: false,
    };
    const uploadApp = express();
    uploadApp.use(cors());
    uploadApp.use(passport.authorize('jwt', jwtConfig));
    uploadApp.all('*', server.handle.bind(server));
    app.use('/uploads', uploadApp);
};
