'use strict';
module.exports = (sequelize, DataTypes) => {
  const GenreTrack = sequelize.define('GenreTrack', {
    genreId: DataTypes.INTEGER,
    trackId: DataTypes.INTEGER
  }, {});
  GenreTrack.associate = function(models) {
    // associations can be defined here
  };
  return GenreTrack;
};