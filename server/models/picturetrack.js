'use strict';
module.exports = (sequelize, DataTypes) => {
  const PictureTrack = sequelize.define('PictureTrack', {
    pictureId: DataTypes.INTEGER,
    trackId: DataTypes.INTEGER
  }, {});
  PictureTrack.associate = function(models) {
    // associations can be defined here
  };
  return PictureTrack;
};