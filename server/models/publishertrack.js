'use strict';
module.exports = (sequelize, DataTypes) => {
  const PublisherTrack = sequelize.define('PublisherTrack', {
    publisherId: DataTypes.INTEGER,
    trackId: DataTypes.INTEGER
  }, {});
  PublisherTrack.associate = function(models) {
    // associations can be defined here
  };
  return PublisherTrack;
};