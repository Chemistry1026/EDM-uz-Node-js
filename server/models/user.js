'use strict';
const {Op} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER,
        },
        username: {
            allowNull: false,
            type: DataTypes.STRING,
        },
        password: {
            allowNull: false,
            length: 255,
            type: DataTypes.STRING,
        },
        firstName: {
            type: DataTypes.STRING,
        },
        lastName: {
            type: DataTypes.STRING,
        },
        email: {
            allowNull: false,
            type: DataTypes.STRING,
        },
        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
        },

        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
        },
        disableAt: {
            type: DataTypes.DATE
        },
        status: {
            type: DataTypes.ENUM('active', 'inactive'),
            defaultValue: 'active',
        }
    }, {
        scopes: {
            login(login) {
                return {
                    where: {
                        [Op.or]: [
                            {email: login},
                            {username: login}
                        ],
                    },
                }
            }
        }
    });
    User.associate = function (models) {
        // associations can be defined here
    };

    return User;
};
