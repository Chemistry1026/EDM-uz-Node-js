'use strict';
module.exports = (sequelize, DataTypes) => {
    const Track = sequelize.define('Track', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: DataTypes.INTEGER
        },

        artists: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        date: {
            type: DataTypes.STRING,
            allowNull: false,
        },

        bpm: {
            type: DataTypes.INTEGER,
        },

        description: {
            type: DataTypes.TEXT,
        },

        lyric: {
            type: DataTypes.TEXT,
        },

        published: {
            type: DataTypes.BOOLEAN,
        },

        userId: {
            type: DataTypes.INTEGER,
        },

        pictureId: {
            type: DataTypes.INTEGER,
        },

        fileId: {
            type: DataTypes.INTEGER,
        },

        createdAt: {
            allowNull: false,
            type: DataTypes.DATE,
        },

        updatedAt: {
            allowNull: false,
            type: DataTypes.DATE,
        },
    }, {
        indexes: [{
            unique: false,
            fields: ['userId', 'fileId', 'pictureId', 'artists', 'name'],
        }],
    });

    Track.associate = function (models) {
        Track.belongsTo(models.File, {
            as: 'file',
        });

        Track.belongsTo(models.User, {
            as: 'user',
        });

        Track.belongsTo(models.Picture, {
            as: 'picture',
        });

        Track.belongsToMany(models.Artist, {
            through: 'ArtistTrack'
        });

        Track.belongsToMany(models.Publisher, {
            through: 'PublisherTrack',
        });

        Track.belongsToMany(models.Genre, {
            through: 'GenreTrack',
        });
    };

    return Track;
};
