'use strict';
module.exports = (sequelize, DataTypes) => {
  const Artist = sequelize.define('Artist', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },

    name: {
      type: DataTypes.STRING
    },

    description: {
      type: DataTypes.TEXT
    },

    date: {
      type: DataTypes.STRING
    },

    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },

    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    }
  }, {});
  Artist.associate = function(models) {
    // associations can be defined here
  };
  return Artist;
};
