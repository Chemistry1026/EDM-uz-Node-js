'use strict';

module.exports = (sequelize, DataTypes) => {
  const Album = sequelize.define('Album', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING,
    date: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});

  Album.associate = function(models) {
    // associations can be defined here
  };
  return Album;
};
