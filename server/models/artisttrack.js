'use strict';
module.exports = (sequelize, DataTypes) => {
  const ArtistTrack = sequelize.define('ArtistTrack', {
    artistId: DataTypes.INTEGER,
    trackId: DataTypes.INTEGER
  }, {});
  ArtistTrack.associate = function(models) {
    // associations can be defined here
  };
  return ArtistTrack;
};