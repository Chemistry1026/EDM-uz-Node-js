'use strict';
let {validateURL, url} = require('../utils/functions');
module.exports = (sequelize, DataTypes) => {
    const File = sequelize.define('File', {
        name: DataTypes.STRING,
        path: DataTypes.STRING,
        // path: {
        //     type: DataTypes.STRING,
        //     get() {
        //         let name = this.getDataValue('name');
        //         let path = this.getDataValue('path');
        //         if(!validateURL(path)){
        //             path = url(path);
        //         }
        //         this.setDataValue('fullPath', `${path}${name}`);
        //         return path;
        //     }
        // },
    }, {
        hooks: {
            afterFind: file => {
                if(!validateURL(file.path)){
                    file.path = url(file.path);
                }
                file.setDataValue('fullPath', `${file.path}${file.name}`);
            },
        }
    });

    File.associate = function (models) {

    };
    return File;
};
