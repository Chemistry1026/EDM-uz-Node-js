const User = require('../../models').User;

module.exports = (req) => {
    req.checkBody('username')
        .notEmpty().withMessage('Поле логин обязательное для заполнения')
        .isAlphanumeric().withMessage('Неверный формат логина')
        .custom(value => {
            return User.findOne({
                where: {
                    username: value
                },
            }).then((user) => {
                return false;
            });
        }).withMessage('Данный логин занят');

    req.checkBody('email')
        .notEmpty().withMessage('Поле email обязательное для заполнения')
        .isEmail().withMessage('Поле email должно быть действительным электронным адресом.')
        .custom(value => {
            return User.findOne({
                where: {
                    email: value
                }
            }).then((user) => {
                if (user) {
                    return false;
                }
            });
        }).withMessage('Данный email занят');

    req.checkBody('password')
        .notEmpty().withMessage('Поле пароль обязательное для заполнения')
        .isLength({
            min: 6,
            max: 50,
        }).withMessage('Минимальная длина пароля от 6 - 50 символов');

    req.checkBody('password_confirmation')
        .notEmpty().withMessage('Поле пароль обязательное для заполнения')
        .equals(req.body.password).withMessage('Поле пароль не совпадает');

    return req.validationErrors();
};
