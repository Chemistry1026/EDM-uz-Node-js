require('dotenv').config();
const User = require('../../models').User;
const { json_data, checkPassword, makeJWT, hashPassword } = require('../../utils/functions');

module.exports = {
    create(req, res) {
        const request = require('../../requests/frontent/UserCreateRequest')(req);
        let { email, password, username } = req.body;
        return hashPassword(password).then((hash) => {
            User.create({
                email,
                username,
                password: hash
            })
            .then(user => res
                .status(200)
                .json(json_data(user, 'Вы успешно зарегистрировались')))
            .catch(error => res
                .status(400)
                .json(json_data(error, 'Ошибка при создании пользователя')));
        });
    },

    async login(req, res){
        let form = req.body;
        await User
            .scope({method: ['login', form.username]})
            .findOne()
            .then(async userData => {
                let user = userData.dataValues;
                let { password } = user;
                await checkPassword(form.password, password).then(result => {
                    let response = makeJWT(user.id);
                    return res.status(200).json(response);
                }).catch(err => {
                    return res.status(404).json(json_data({}, 'Ошибка неверный пароль'));
                });
            }).catch(err => {
                return res.status(404).json(json_data({}, 'Ошибка пользователь не найден'));
            });

    },

    user(req, res){
        return res.status(200).json({
            user: req.account,
        });
    }
};
