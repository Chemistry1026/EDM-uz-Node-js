require('dotenv').config();

const User = require('../../models').User;
const resumable = require('../../utils/resumable')('uploads/temp/', 'uploads/');
const crypto = require('crypto');

module.exports = {
    getFileID(req, res){
        if(!req.query.filename){
            return res.status(500).end('query parameter missing');
        }
        // create md5 hash from filename
        res.end(
            crypto.createHash('md5')
                .update(req.query.filename)
                .digest('hex')
        );
    },

    upload(req, res){
        req.setTimeout(4 * 60 * 1000);
        resumable.post(req, function(status, filename, original_filename, identifier){
            console.log('POST', status, original_filename, identifier);
            res.send(status);
        });
    },

    complete(req, res){
        resumable.complete(req, (status) => {
            res.send(status);
        });
    },

    checkChunk(req, res){
        resumable.get(req, function(status, filename, original_filename, identifier){
            console.log('GET', status);
            res.send((status === 'found' ? 200 : 404), status);
        });
    },

    download(req, res){
        resumable.write(req.params.identifier, res);
    }
};
