const { Artist, Genre, Publisher, Track, File, Picture } = require('../../models');
const { json_data } = require('../../utils/functions');

module.exports = {
    index(req, res) {
        // Artist.bulkCreate([
        //     {
        //         name: 'Quintino',
        //     },
        //
        //     {
        //         name: 'Sullivan King',
        //     },
        //
        //     {
        //         name: 'Radiology',
        //     },
        //
        //     {
        //         name: 'Dada Life',
        //     },
        //
        //     {
        //         name: 'Dropgun',
        //     },
        //
        //     {
        //         name: 'Going Deeper',
        //     },
        //
        //     {
        //         name: 'Mingue',
        //     },
        //
        //     {
        //         name: 'Mingue',
        //     },
        //
        //     {
        //         name: 'Timmy Trumpet',
        //     },
        //
        //     {
        //         name: 'Dimatik',
        //     },
        //
        //     {
        //         name: 'Bellorum',
        //     },
        //
        //     {
        //         name: 'Aazar',
        //     },
        //
        //     {
        //         name: 'Alvaro',
        //     },
        //
        //     {
        //         name: 'Breathe Carolina',
        //     },
        //
        //     {
        //         name: 'KEVU',
        //     },
        //
        //     {
        //         name: 'Starx & Distort',
        //     },
        // ]);
        // Genre.bulkCreate([
        //     {
        //         name: 'Electro House',
        //     },
        //
        //     {
        //         name: 'Big Room',
        //     },
        //
        //     {
        //         name: 'House',
        //     },
        //
        //     {
        //         name: 'Future House',
        //     },
        // ]);
        // Publisher.bulkCreate([
        //     {
        //         name: 'Spinnin\' Records',
        //     },
        //
        //     {
        //         name: 'Dharma Worldwide',
        //     },
        //
        //     {
        //         name: 'Flamingo Recordings',
        //     },
        // ]);
        // Picture.bulkCreate([
        //     {
        //         name: '1523796020-going-deepermingue-hold-on-original-mix.jpg',
        //         path: 'uploads/stored/1/',
        //     },
        //
        //     {
        //         name: '1523795772-dropgun-together-as-one-original-mix.jpg',
        //         path: 'uploads/stored/2/',
        //     },
        //
        //     {
        //         name: '1523791430-dada-life-one-nation-under-lasers-original-mix.jpg',
        //         path: 'uploads/stored/3/',
        //     },
        //
        //     {
        //         name: '1523719749-radiology-phobia.jpg',
        //         path: 'uploads/stored/4/',
        //     },
        // ]);

        // File.bulkCreate([
        //     {
        //         name: '5b00453c-13c9-4f12-9264-b914fbee1477-128.mp3',
        //         path: 'uploads/stored/1/',
        //     },
        //
        //     {
        //         name: 'accf0b4c-75b4-4bfa-a980-ca2a2b4e9c4c-128.mp3',
        //         path: 'uploads/stored/2/',
        //     },
        //
        //     {
        //         name: '19423b30-0d87-4b1e-a1bd-ae1c7396751f-128.mp3',
        //         path: 'uploads/stored/3/',
        //     },
        //
        //     {
        //         name: 'c47a07b7-53ef-455c-a947-f9d4aae27fb6-128.mp3',
        //         path: 'uploads/stored/4/',
        //     },
        // ]);
        File.findOne().then(file => {
            return res.status(200).json(json_data(file));
        });
    },

    create(req, res) {

    },

    store(req, res) {

    },

    show(req, res) {

    },

    edit(req, res) {

    },

    update(req, res) {

    },

    destroy(req, res) {

    },
};
