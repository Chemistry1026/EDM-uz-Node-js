'use strict';

const {hashPassword, makeFaker} = require('../utils/functions');
let users = [];
module.exports = {
    up: (queryInterface, Sequelize) => {
        return new Promise(async (resolve, reject) => {
            users = await makeFaker(async (faker) => {
                let firstName = faker.name.firstName();
                let lastName = faker.name.lastName();
                let number = faker.random.number();
                let username = `${firstName}_${lastName}_${number}`;
                let email = faker.internet.email();
                let result = {};

                await hashPassword(username).then(hash => {
                    result = {
                        username,
                        password: hash,
                        firstName,
                        lastName,
                        email,
                    };
                }).catch(err => {
                    console.log(err);
                    reject(err);
                });
                return result;
            }, 10);

            resolve(users);
        }).then((result) => {
            return queryInterface.bulkInsert('Users', result, {});
        });
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkInsert('People', [{
            name: 'John Doe',
            isBetaMember: false
          }], {});
        */
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('People', null, {});
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.

          Example:
          return queryInterface.bulkDelete('People', null, {});
        */
    }
};
