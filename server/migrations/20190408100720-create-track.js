'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Tracks', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },

            artists: {
                type: Sequelize.STRING,
                allowNull: false,
            },

            name: {
                type: Sequelize.STRING,
                allowNull: false,
            },

            date: {
                type: Sequelize.STRING,
                allowNull: false,
            },

            bpm: {
                type: Sequelize.INTEGER,
            },

            description: {
                type: Sequelize.TEXT,
            },

            lyric: {
                type: Sequelize.TEXT,
            },

            published: {
                type: Sequelize.BOOLEAN,
            },

            userId: {
                type: Sequelize.INTEGER,
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
            },

            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                onUpdate: Sequelize.literal('CURRENT_TIMESTAMP'),
            }
        });
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Tracks');
    }
};
