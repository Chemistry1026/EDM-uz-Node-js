const frontRoute = require('./front');
const backRoute = require('./back');
module.exports = {
    frontRoute,
    backRoute,
};
