const cors = require('cors');
const passport = require('passport');

const FrontendUsersController = require('../controllers/frontend/users');
const FrontendUploaderController = require('../controllers/frontend/uploader');
const TestContoller = require('../controllers/frontend/test');

const config = {
    session: false,
};

module.exports = (app) => {
    app.group('/front', router => {
        router.use(cors());
        router.get('/', TestContoller.index);
        router.post('/login', FrontendUsersController.login);
        router.post('/register', FrontendUsersController.create);

        router.group('/user', userRouter => {
            userRouter.use(passport.authorize('jwt', config));
            userRouter.get('/', FrontendUsersController.user);

            userRouter.group('/upload', uploadRouter => {
                uploadRouter.post('/', FrontendUploaderController.upload);
                uploadRouter.post('/complete', FrontendUploaderController.complete);
                uploadRouter.get('/fileid', FrontendUploaderController.getFileID);
                uploadRouter.get('/', FrontendUploaderController.checkChunk);
                uploadRouter.get('/download/:identifier', FrontendUploaderController.download);
            });
        });
    });
};
