const cors = require('cors');
module.exports = (app) => {
    app.group('back', router => {
        router.use(cors());
    });
};
