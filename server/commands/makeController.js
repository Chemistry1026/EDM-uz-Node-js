require('dotenv').config();
const fs = require('fs');
const path = require('path');

const controllerPath = process.env.CONTROLLER_PATH;
const defaultController = `${controllerPath}/controller.js`;

const [, , ...args] = process.argv;

args.forEach(item => {
    let file = `${controllerPath}/${item}`;
    let {dir, name} = path.parse(file);
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    fs.copyFileSync(defaultController, `${file}.js`, err => {
        if (err) throw err;
    });

    console.log(`Контроллер: ${name} создан`);
});
