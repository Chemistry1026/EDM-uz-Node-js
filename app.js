const express = require('express');
const logger = require('morgan');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const passport = require('./server/utils/passport');
const expressValidator = require('express-validator');
const rateLimit = require("express-rate-limit");
const multipart = require('connect-multiparty');
require('dotenv').config();

require('./server/utils/express')(express);
const app = express();

// require('./server/utils/uploader')(express, app);


app.enable("trust proxy"); // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100 // limit each IP to 100 requests per windowMs
});

// app.use(limiter);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(expressValidator());
app.use(multipart());

app.use(session({
    secret: process.env.SESSION_SECRET,
    cookie: {
        maxAge: 900000,
    },
    resave: false,
    saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());
app.use('/files', express.static('uploads'));

const router = require('./server/routes');
router.frontRoute(app);
router.backRoute(app);

app.get('*', (req, res) => res.status(404).send({
    message: 'Ooops, page not found!',
}));

module.exports = app;
